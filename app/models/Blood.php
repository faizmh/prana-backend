<?php
use Phalcon\Db\RawValue;
class Blood extends \Phalcon\Mvc\Model {
	public function initialize()
	{
		$this->useDynamicUpdate(true);
	}
	public function beforeCreate()
	{
		$this->reported_dt = new RawValue('default');
	}
}
