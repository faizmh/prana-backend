<?php
use Phalcon\Db\RawValue;
class User extends \Phalcon\Mvc\Model {
	public function initialize()
	{
		$this->useDynamicUpdate(true);
	}
	public function beforeCreate()
	{
		$this->added_dt = new RawValue('default');
	}
}
