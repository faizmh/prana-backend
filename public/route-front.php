<?php
use Phalcon\Http\Request;

if (!defined('XAppInit')) {
	die('Direct access not permitted');
}

$app->get('/', function () use ($app) {
	echo 'Bit bucket Frontend Repo here <br/> <a href="http://bitbucket.org/faizmh/prana-frontend" target="_blank">prana-frontend</a> <br/>';
	echo '<br/>';
	echo 'Bit bucket Backend Repo here <br/> <a href="http://bitbucket.org/faizmh/prana-backend" target="_blank">prana-backend</a> <br/>';
});
$app->post('/profile_addl/{device_token}', function ($device_token) use ($app) {
	$request = new Request();
	$blood_group = $request->getPost("blood_group");
	$share_my_location = $request->getPost("share_my_location");
	$latitude = $request->getPost("latitude");
	$longitude = $request->getPost("longitude");
	
	$response = array(
		'meta' => array(
			'status' => 'OK',
			'code' => 200,
			'methodName' => 'profileAddl',
		)
	);
	
	$user = User::findFirst('device_token = \''. $device_token .'\'');
	$user->blood_group = $blood_group;
	$user->latitude = $latitude;
	$user->longitude = $longitude;
	$user->enable_ping = isset($share_my_location) ? true :false;
	$user->save();
	$response['user'] = $user;
	return jsonResponse(200, $response);
});


$app->post('/event/{device_token}', function ($device_token) use ($app) {
	$request = new Request();
	$latitude = $request->getPost("latitude");
	$longitude = $request->getPost("longitude");
	$comments = $request->getPost("comments");
	$user = User::findFirst('device_token = \''. $device_token .'\'');
	
	$response = array(
		'meta' => array(
			'status' => 'OK',
			'code' => 200,
			'methodName' => 'createEvent',
		)
	);
	
	$event = new Event();
	$event->latitude = $latitude;
	$event->longitude = $longitude;
	$event->user_id = $user->id;
	$event->comments = $comments;
	$event->save();
	$response['event'] = $event;
	$param = array();
	$param['event_id'] = $event->id;
	notify($param);
	return jsonResponse(200, $response);
	
});
$app->post('/blood/{device_token}', function ($device_token) use ($app) {
	$request = new Request();
	$latitude = $request->getPost("latitude");
	$longitude = $request->getPost("longitude");
	$comments = $request->getPost("comments");
	$blood_group = $request->getPost("blood_group");
	$user = User::findFirst('device_token = \''. $device_token .'\'');

	$response = array(
		'meta' => array(
			'status' => 'OK',
			'code' => 200,
			'methodName' => 'createBloodRequest',
		)
	);

	$blood = new Blood();
	$blood->latitude = $latitude;
	$blood->longitude = $longitude;
	$blood->user_id = $user->id;
	$blood->comments = $comments;
	$blood->blood_group = $blood_group;
	$blood->save();
	$response['event'] = $blood;
	$param = array();
	$param['blood_id'] = $blood->id;	
	notify($param);
	return jsonResponse(200, $response);

});
	
$app->post('/user', function () use ($app) {
    $request = new Request();
    $phoneNum = $request->getPost("phone_num");
    $deviceToken = $request->getPost("device_token");

    $response = array(
        'meta' => array(
            'status' => 'OK',
            'code' => 200,
            'methodName' => 'userSignup',
        )
    );
    // check whether this user exists or not
    $user = User::findFirst('phone_num = '. $phoneNum);
    $verificationCode = 123; //rand(100000, 999999);
    if ($user) {
        $user->verification_code = $verificationCode;
        $user->save();
        $user->membership_status = 'Existing';
        $response['user'] = $user;
        sendSMS($phoneNum,$verificationCode);
    } else {
        $user = new User();
        $user->phone_num = $phoneNum;
        $user->device_token = $deviceToken;
        $user->verification_code = $verificationCode;
        $user->save();
        $user->membership_status = 'New';
		sendSMS($phoneNum,$verificationCode);
        
        $response['user'] = $user;
    }
    /* send SMS to this mobile number with verification code */
    // sendSMS();

    return jsonResponse(200, $response);
});

function sendSMS($phone, $verfication_code){
	return;
	$username = getenv('sms_user'); 
	$password = getenv('sms_password'); 
	$sender = "PROPOK";
	$recipient = $phone;
	$message = " Your verification code : " . $verfication_code;
	$message = urlencode ($message );
	$parms = 'user='.$username.'&pass='.$password.'&sender='.$sender.'&phone='.$recipient.'&text='.$message.'&priority=sdnd&stype=normal';
	
	$Curl_Session = curl_init('http://bhashsms.com/api/sendmsg.php?' . $parms);
	curl_setopt ($Curl_Session, CURLOPT_FOLLOWLOCATION, 1);
	curl_setopt($Curl_Session, CURLOPT_RETURNTRANSFER,1);
	$result = curl_exec ($Curl_Session);
	curl_close ($Curl_Session);
}

$app->get('/alert', function () use ($app) {
	
	$sql = 'select \'event\' as type, reported_dt,comments from event'.
			' union '.
			'select \'blood\' as type, reported_dt, concat(blood_group,\' \', comments) from blood'.
			' order by reported_dt desc';
	$result = $app->db->query($sql);
	return jsonResponse(200, $result->fetchAll());
});

$app->put('/user/{user_id}', function ($userId) use ($app) {
    $response = array(
        'meta' => array(
            'status' => 'OK',
            'code' => 200,
            'methodName' => 'userSignup',
        )
    );
    $user = User::findFirst('id = '. $userId);
    if ($user) {
        $user->blood_group_id = $request->getPost("blood_group_id");
        $user->latitude = $request->getPost("latitude");
        $user->longitude = $request->getPost("longitude");
        $user->enable_ping = $request->getPost("enable_ping");
        $user->save();
        $response['user'] = $user;
        return jsonResponse(200, $response);
    } else {
        //return jsonResponse(200, $response);
    }
});



function notify($param) {

	
    //$apnsHost = 'gateway.push.apple.com';
    $apnsHost = 'gateway.sandbox.push.apple.com';
    $apnsPort = 2195;
    $apnsCert = '../apns/APNS_dev.pem';
    $passphrase = '';

    $ctx = stream_context_create();
    stream_context_set_option($ctx, 'ssl', 'local_cert', $apnsCert);
    stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
    $apns = stream_socket_client(
        'ssl://' . $apnsHost . ':' . $apnsPort,
        $error,
        $errorString,
        2,
        STREAM_CLIENT_CONNECT,
        $ctx
    );
    stream_set_blocking($apns, 0);

    $latitude = 0.0;
    $longitude = 0.0;
    $msg = 'Message';
    if(isset($param['event_id'])){
    	$event = Event::findFirst($param['event_id']);
    	$msg = $event->comments;
    	$latitude = $event->latitude;
    	$longitude = $event->longitude;
    } else {
    	$blood = Blood::findFirst($param['blood_id']);
    	$msg = $blood->blood_group. ' ' .$blood->comments;
    	$latitude = $blood->latitude;
    	$longitude = $blood->longitude;
    }
    
    if ($apns) {
        $payload['aps'] = array(
            'alert' => $msg,
            'badge' => 0,
            'sound' => 'default',
        	'latitude' => $latitude,
        	'longitude' => $longitude
        );
        $data = json_encode($payload);

        $total = 250;
        $len = strlen($data);
        
        if ($len > $total) {
            /* limit alert message */
            $exeptDataLen = $len - strlen($payload['aps']['alert']);
            if ($exeptDataLen > $total) {
                /* data is too large */
                return array('success' => false, 'message' => 'Data is too large');
            } else {
                $availabledataLen = $total - $exeptDataLen;
                //$payload['aps']['alert'] = substr($payload['aps']['alert'], 0, $availabledataLen);
                $data = json_encode($payload);
            }
        }
        $users = User::find();
        $idArr = array();
        foreach ($users as $user){
        	array_push($idArr,$user->device_token);
        }
        $deviceTokens = $idArr;

        foreach ($deviceTokens as $token) {
            if (isset($notificationCountArr[$token])) {
                $payload['aps']['badge'] = $notificationCountArr[$token];
                $data = json_encode($payload);
            } else {
                $payload['aps']['badge'] = 1;
                $data = json_encode($payload);
            }
            $apple_identifier = 1234;
            $apple_expiry = time() + (90 * 24 * 60 * 60);
            $cleanToken = str_replace(' ', '', $token);
            //$apnsMessage = chr(0) . chr(0) . chr(32) . @pack('H*', str_replace(' ', '', $token)) . chr(0) . chr(strlen($data)) . $data;
            $apnsMessage = pack("C", 1) .
                pack("N", $apple_identifier) .
                pack("N", $apple_expiry) .
                pack("n", 32) .
                pack("H*", $cleanToken) .
                pack("n", strlen($data)) . $data; //Enhanced Notification
                fwrite($apns, $apnsMessage);
            usleep(500000);
            /*if (checkAppleErrorResponse($apns)) {
                $ctx = stream_context_create();
                stream_context_set_option($ctx, 'ssl', 'local_cert', $apnsCert);
                stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
                $apns = stream_socket_client('ssl://' . $apnsHost . ':' . $apnsPort, $error, $errorString, 2, STREAM_CLIENT_CONNECT, $ctx);
                stream_set_blocking($apns, 0);
            }*/
        }
        fclose($apns);
        return array('success' => true);
    }
    return array('success' => false, 'message' => 'Socket connection failed');
};
